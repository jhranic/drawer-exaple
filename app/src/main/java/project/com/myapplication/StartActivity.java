package project.com.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

public class StartActivity extends AppCompatActivity implements Animation.AnimationListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        ImageView imageContent = (ImageView) findViewById(R.id.image_content);
        Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.start);
        animation.setAnimationListener(this);
        imageContent.startAnimation(animation);

    }

    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {
        findViewById(R.id.image_content).setVisibility(View.INVISIBLE);
        startActivity(new Intent(this, DrawerActivity.class));
        finish();
    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }
}